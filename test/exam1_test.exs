defmodule Exam1Test do
  use ExUnit.Case
  doctest Exam1

  @tag :skip
  test "Median" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.median(l1) == Statistics.median(l1)
    assert Exam1.median(l2) == Statistics.median(l2)
  end

  @tag :skip
  test "Q1" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.quartile1(l1) == Statistics.quartile(l1, :first)
    assert Exam1.quartile1(l2) == Statistics.quartile(l2, :first)
  end

  @tag :skip
  test "Q3" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.quartile3(l1) == Statistics.quartile(l1, :third)
    assert Exam1.quartile3(l2) == Statistics.quartile(l2, :third)
  end

  @tag :skip
  test "IQR" do
    l1 = [30, 20, 14, 4, 1, 16, 20, 3, 10, 23, 36]
    l2 = [2, 44, 28, 46, 25, 24, 20, 23, 12, 11, 43, 16, 45, 11, 29, 26]
    assert Exam1.iqr(l1) == Statistics.iqr(l1)
    assert Exam1.iqr(l2) == Statistics.iqr(l2)
  end
end
